% Grid-refinement in Palabos
% Palabos summer school - O. Malaspinas
% July 9, 2020

\newcommand{\p}{\partial}
\newcommand{\uu}{\vec u}
\newcommand{\ux}{\vec x}
\newcommand{\taubar}{\bar \tau}
\newcommand{\uc}{\vec c}
\newcommand{\uS}{\underline{\underline S}}
\newcommand{\uT}{\underline{\underline T}}
\newcommand{\uI}{\underline{\underline I}}
\newcommand{\fzero}{f^{(0)}}
\newcommand{\fneq}{f^\mathrm{neq}}
\newcommand{\fbarneq}{\bar f^\mathrm{neq}}
\newcommand{\fbarneqc}{\bar f^\mathrm{neq,c}}
\newcommand{\fbarneqf}{\bar f^\mathrm{neq,f}}
\newcommand{\fbar}{\bar f}

\Huge Don't hesitate to interrupt!

# Grid-refinement generalities (1/2)

## The need for variable resolution

![Source: Wikipedia, <https://bit.ly/38l3Kor>](figs/Laminar-turbulent_transition.png){#fig:multi width=80%}

# Grid-refinement generalities (2/2)

## Basics of grid refinement

* Geometrical considerations[^2]

\begin{tabular}{c c}
\textbf{Multi--grid} & \textbf{Multi--domain}\\
& \\
\includegraphics[height=0.3\textheight]{figs/multiGrid.pdf} & 
\includegraphics[height=0.3\textheight]{figs/multiDomain.pdf}\\
\end{tabular}

* In Palabos: multi-domain[^1]

[^1]: D. Lagrava et al., Advances in multi-domain lattice Boltzmann grid refinement, J. Comp. Phys., 231, p. 4808-4822, (2012)

[^2]: P. Sagaut, et. al, Multiscale And Multiresolution Approaches in Turbulence, Imperial College Press, June 2006.


# Basics of grid refinement (1/2)

* The discretization LBM performed over a regular grid
* Introduction of non--uniform structure
  * Discontinuity of the physical quantities
  * Quantities must be rescaled
* Transitions are only powers of two 
   
  ![](figs/twoGrids.pdf){#fig:multi width=80%}
 
# Basics of grid refinement (2/2)

* Coarse grid: $p_c$, $\uu_c$, $\uS_c$, \dots
* Fine grid: $p_f$, $\uu_f$, $\uS_f$, \dots
  
  ![](figs/twoGrids_macro.pdf){#fig:twog width=100%}
* Related via physical units: $p$, $\uu$, $\uS$, \dots
  \begin{align*}
  &\mbox{Pressure: } &p = \frac{\delta x_c^2}{\delta t_c^2} p_c  = \frac{\delta x_f^2}{\delta t_f^2} p_f,\\
  &\mbox{Velocity: } &\uu = \frac{\delta x_c}{\delta t_c} \uu_c  = \frac{\delta x_f}{\delta t_f} \uu_f,\\
  &\mbox{Strain: } &\uS = \frac{1}{\delta t_c} \uS_c = \frac{1}{\delta t_f} \uS_f.
  \end{align*}

# Questions on basics

\Huge Questions?

# Rescaling of macroscopic quantities[^3] (1/2)

## Density, pressure, and velocity

* We consider the convective scaling $\delta t\sim\delta x$.
  * Meaning $\delta x_c=\delta x_f/2$ then $\delta t_c=\delta t_f/2$.
  * There are **more** time-steps on the fine lattice.
  * $p$, $\uu$, and $\rho$ are continuous at the interface.
* Pressure and Density ($p=c_s^2\rho$):
  \begin{align*}
  \frac{\delta x_f^2}{\delta t_f^2} p_f=\frac{4\delta x_c^2}{4\delta t_c^2} p_f&=\frac{\delta x_c^2}{\delta t_c^2} p_c,\\
  p_f&=p_c\Leftrightarrow \rho_f=\rho_c.
  \end{align*}
* Velocity:
  \begin{align*}
  \frac{\delta x_f}{\delta t_f} \uu_f=\frac{2\delta x_c}{2\delta t_c} \uu_f&=\frac{\delta x_c}{\delta t_c} \uu_c,\\
  \uu_f&=\uu_c.
  \end{align*}

[^3]: A. Dupuis, B. Chopard, Theory and applications of an alternative lattice Boltzmann grid refinement algorithm, Physical Review E, 67 (2003), p. 066707.

# Rescaling of macroscopic quantities (2/2)

## Rate of strain tensor

* We consider the convective scaling $\delta t\sim\delta x$.
  * Meaning $\delta x_c=\delta x_f/2$ then $\delta t_c=\delta t_f/2$.
  * $p$, $\uu$, and $\rho$ are continuous at the interface ($\uu_f=\uu_c$, ...).
* Strain:
  \begin{align*}
  \frac{1}{\delta t_f} \uS_f=\frac{2}{\delta t_c} \uS_f&=\frac{1}{\delta t_c} \uS_c,\\
  \uS_f&=\frac{1}{2}\uS_c.
  \end{align*}

# Rescaling of populations (1/2)

## Three different parts: $f_i$, $\fzero_i$, and $\fneq_i$

* Populations are represented as $f_i=\fzero_i+\fneq_i$
    $$
    \fzero_i=w_i\rho\left(1+\frac{\uc_i\cdot \uu}{c_s^2}+\frac{1}{2c_s^4}(\uc_i\uc_i-c_s^2\uI):\uu\uu)\right).
    $$
* We know $\rho_c=\rho_f$, $\uu_c=\uu_f$.
* Equilibrium pop:
  $$
  \fzero_{i,c}=\fzero_{i,f}.
  $$
* Non-equilibrium pop:
  * $\fneq_i = f_i-\fzero_i$ is not continuous.
  * $\fneq_{i,c} = \alpha \fneq_{i,f}$.

# Rescaling of populations (2/2)

## Let us start with the BE

BE equation with BGK approximation ($f(\ux,\uc,t)$)
\begin{equation*}
(\p_t + \uc\cdot \vec\nabla_{\ux} ) f = -\frac{1}{\tau}(f-\fzero) 
\end{equation*}

## Velocity discretization

Finite velocity BE equation ($f(\ux,\uc_i,t) \equiv f_i(\ux,t)$)
\begin{equation*}
(\p_t + \uc_i \cdot \vec\nabla_{\ux} ) f_i = -\frac{1}{\tau}(f_i-\fzero_i)  
\end{equation*}
We rewrite it
\begin{equation*}
\frac{\mathrm{d}f_i}{\mathrm{d}t} = -\frac{1}{\tau}(f_i-\fzero_i)
\end{equation*}

# *A priori* determination of $\alpha$ (1/2)

## Space-time discretization 

After numerical integration along characteristics
\begin{equation*}
  f_i^+-f_i=-\frac{\delta t}{2\tau}\left(f_i^+-{\fzero_i}^++f_i-\fzero_i\right),
\end{equation*}
"+" : function evaluated at position $\ux+\uc_i\delta t$ and time $t+\delta t$.
With the change of variable
\begin{align*}
  \fbar_i&=f_i+\frac{\delta t}{2\tau}\left(f_i-\fzero_i\right),\\
  \taubar&=\frac{2\tau+\delta t}{2\delta t},
\end{align*}
we obtain
\begin{equation*}
  \fbar_i^+=\fbar_i-\frac{1}{\taubar}\left(\fbar_i-\fzero_i\right).
\end{equation*}

# A priori determination of $\alpha$ (2/2)

## Non-equilibrium distribution

Subtracting $\fzero_i$ from $\fbar_i=f_i+\frac{\delta t}{2\tau}\left(f_i-\fzero_i\right)$
\begin{equation*}
  \fbarneq_i=\left(\frac{2\tau+\delta t}{2\tau}\right)\fneq_i\Leftrightarrow \fneq_i=\left(\frac{2\tau}{2\tau+\delta t}\right)\fbarneq_i,
\end{equation*}

## Continuity of $\fneq_i$

Ensure continuity of "bare" quantities on coarse and fine grid
\begin{align*}
  \left(\frac{2\tau}{2\tau+\delta t_c}\right)\fbarneq_{i,c}&=\left(\frac{2\tau}{2\tau+\delta t_f}\right)\fbarneq_{i,f},\\
  \frac{\delta t_c}{\taubar_c}\fbarneqc_i&=\frac{\delta t_f}{\taubar_f}\fbarneqf_i,\\
  \alpha &= \frac{\delta t_f}{\delta t_c} \frac{\taubar_c}{\taubar_f} 
\end{align*}

# Rescaling in Palabos (1/4)

## From coarse to fine

* Decompose: from $f_{i,c}$, compute $\rho_c$, $\uu_c$, $\fneq_{i,c}$.
  $$
  \rho_c=\sum_i f_{i,c}, \quad \uu_c=\sum_i f_{i,c}\uc_i/rho_c,\quad \fneq_{i,c}=f_{i,c}-\fzero_{i,c}.
  $$
* Rescale: 
  $$
  \rho_f=\rho_c,\quad \uu_f=\uu_c,\quad \fneq_{i,f}=\fneq_{i,c}/\alpha.
  $$
* Recompose: from $\rho_f$, $\uu_f$, $\fneq_{i,f}$ compute $f_{i,f}$.
  $$
  f_{i,f}=\fzero_i(\rho_f,\uu_f)+\fneq_{i,f}.
  $$

# Rescaling in Palabos (2/4)

## From coarse to fine

* Decompose: from $f_{i,f}$, compute $\rho_f$, $\uu_f$, $\fneq_{i,f}$.
  $$
  \rho_f=\sum_i f_{i,f}, \quad \uu_f=\sum_i f_{i,f}\uc_i/rho_f,\quad \fneq_{i,f}=f_{i,f}-\fzero_{i,f}.
  $$
* Rescale: 
  $$
  \rho_c=\rho_f,\quad \uu_c=\uu_f,\quad \fneq_{i,c}=\alpha \fneq_{i,f}.
  $$
* Recompose: from $\rho_c$, $\uu_c$, $\fneq_{i,c}$ compute $f_{i,c}$.
  $$
  f_{i,c}=\fzero_i(\rho_c,\uu_c)+\fneq_{i,c}.
  $$

# Rescaling in Palabos (3/4)

## Some code

```C++
class Rescaler {
  // Rescales rel. freq. (potentially many of them)
  virtual Array<T,Descriptor<T>::q> computeRescaledRelFreq(
    const Array<T,Descriptor<T>::q> &relFreq, T xDt) const;
  // Recale the decomposed quantities
  virtual void rescale(const Dynamics<T,Descriptor> &dyn,
    T xDt, std::vector<T> &rawData ) const = 0;

  // Decomposes a cell into rho, u, fneq, and rescales it
  virtual void decomposeAndRescale(
    Cell<T,Descriptor> const& cell, T xDt, plint order,
    std::vector<T> &decompAndRescaled) const;
  // Other things (constructor, ...)
}
```

# Rescaling in Palabos (4/4)

## For BGK (simplified)

```C++
class Rescaler {
  // xDt -> 2 other 1/2
  virtual void rescale(const Dynamics<T,D> &dyn,
    T xDt, std::vector<T> &rawData ) const {
      // rawData[0] = rho, rawData[1-3] = u, 
      // rawData[4-q] = fneq
      Array<T, D<T>::q> resRelFreq = 
        this->computeRescaledRelFreq(relFreq, xDt);
      for (plint iPop = 0; 
           iPop < SymmetricTensorImpl<T,D<T>::d>::n; ++iPop) {
        plint iA = 1+D<T>::d+iPop;
        T prefactor = relFreq[iA] / resRelFreq[iA] * xDt;
        rawData[iA] *= prefactor;
      }
    }
}
```

# Questions on rescaling

\Huge Questions?

# Overlapping zone

## Coupling between refinement zones

* Two way coupling to complete missing information.
 
![](figs/buffering.pdf){#fig:buff width=70%}

* Need for a buffering zone: "overlap".
* In Palabos the thickness is *one* coarse node.

# Overlapping in Palabos (1/3)

:::::::::::::: {.columns}
::: {.column width="50%"}
## Overlap and interface
![](figs/blockStructure.svg){#fig:blockStructure width=100%}

* Communication in overlaps
* Implemention: `NTensorFields`
* Containing `rawData`
* Efficiency vs "ease" of impl.
:::
::: {.column width="50%"}
## Complex overlap
![](figs/blockStructure_complex.svg){#fig:blockStructure width=100%}
:::
::::::::::::::

# Overlapping in Palabos (2/3)

:::::::::::::: {.columns}
::: {.column width="40%"}
## Overlap and interface
![](figs/blockStructure.svg){#fig:blockStructure width=100%}

* Complex data struct.
* Sparse data struct.
* Complex interface geom.
:::
::: {.column width="60%"}
## MultiLevel3D

Each grid level contains

```C++
// Lattice at that level
MultiBlockLattice3D<T, D> *lattice;
// Buffer zones
MultiNTensorField3D<T> *decomp_t0,
                       *decomp_t12, 
                       *decomp_t1, 
                       *decomp_fine;
```
:::
::::::::::::::

**Coupling involves different operations on the interface.**

# Overlapping in Palabos (3/3)

## MultiLevel3D

* `DecomposeAndRescaleFunctional3D`
  ```C++
  Cell<T,Descriptor> &cell = lattice.get(iX,iY,iZ);
  egine.decomposeAndRescale( cell, xDt, order, 
    decompAndRescaled);
  for (iA = 0; iA < decompAndRescaled.size(); ++iA) {
    tensor.get(oX,oY,oZ)[iA] = decompAndRescaled[iA];
  }
  ```
* `RecomposeFunctional3D` 
  ```C++
  Cell<T,Descriptor> &cell = lattice.get(iX,iY,iZ);
  for (iA = 0; iA < nDim; ++iA) {
    decomposed[iA] = 
      tensor.get(oX,oY,oZ)[iA];
  }
  cell.getDynamics().
    recompose(cell, decomposed, order );
  ```

# Two-dimensional interface

![](figs/completeSchema.pdf){#fig:complete width=100%}

# Coarse to fine coupling

## Missing information

* One must "increase" the information.

![](figs/coarseToFine.pdf){#fig:ctof width=60%}

* Copy on superposed nodes.
* Interpolate missing info.
* Temporal **and** spatial interpolations.

# Coarse to fine coupling: temporal interpolation

Need time interpolation for $t=t+\delta t/2$.

![](figs/coarseToFineTime.pdf){#fig:ctof_t width=60%}

Linear time interpolation (second order)
\begin{equation*}
 f_i(\ux,t+\delta t/2)=\frac{f_i(\ux,t+\delta t)+f_i(\ux,t)}{2}.
\end{equation*}


# Coarse to fine coupling: spatial interpolation

## What interpolation ?

* Linear interpolation (second order)
* Cubic interpolation (fourth order)
  ![](figs/interpolationSchemas.pdf){#fig:interp_s width=60%}

* Which one to chose?
  
  **Cubic interpolation.**

# Importance of the spatial interpolation (1/2)

## Second order interpolation is not enough

* Numerical proof using a simple 2D Poiseuille flow
* Compare linear spatial interpolation and cubic spatial interpolation
* Setup of the simulation
  ![](figs/poiseuille.pdf){#fig:poiseuille width=100%}

# Importance of the spatial interpolation (2/2)

## Results

* A linear pressure gradient is expected.
* The pressure gradient of the simulation (both interpolations)
  ![](figs/densityError.pdf){#fig:pressure_prof width=66%}
* There is a loss of mass on the interface (when $\tau\rightarrow 1/2$, or high Re)!
* No more second order accuracy

# Questions on interpolations

\Huge Questions?

# Coarse to fine coupling in Palabos

## Processing functionals

* Couplings is done through Processing Functionals.
* Ordering through negative levels (executed explicitly).

## Spatial processing functionals

* From coarse `MultiNTensorField3D` to fine `MultiNTensorField3D`
  ```C++
  CopyAndSpatialInterpolationPlaneFunctional3D
  CopyAndSpatialInterpolationEdgeFunctional3D
  CopyAndSpatialInterpolationCornerFunctional3D
  ```

## Temporal functionals

* From fine `MultiNTensorField3D` to fine `MultiNTensorField3D`
  ```C++
  TemporalInterpolationFunctional3D
  ```

# Fine to coarse coupling

## Filtering

The fine grid has ``too much'' information

![](figs/fineToCoarse.pdf){#fig:ftoc height=50%}

Averaging over all  lattice directions
$$
\fneq_{i,f}(\ux_{f\rightarrow c}^c,t) = \frac{1}{q}\sum_{i=0}^{q-1} \fneq_{i,f}(\ux_{f\rightarrow c}^c+\uc_i,t)
$$

# Fine to coarse coupling in Palabos

## Filtering functional

```C++
for (plint iA = 0; iA < minIndex; ++iA) {
  // rho and u may not be filtered (only copied)                	
  cTensor.get(iX,iY,iZ)[iA] = fTensor.get(fX,fY,fZ)[iA];
}
for (plint iA = minIndex; // only fneq is
     iA < nDim-D<T>::ExternalField::numScalars; ++iA) {
  cTensor.get(iX,iY,iZ)[iA] = fTensor.get(fX,fY,fZ)[iA]; 
  for (plint iPop = 1; iPop < q; ++iPop) {
    plint nextX = fX+c[iPop][0];
    plint nextY = fY+c[iPop][1];
    plint nextZ = fZ+c[iPop][2];
    cTensor.get(iX,iY,iZ)[iA] += 
      fTensor.get(nextX,nextY,nextZ)[iA]; 
  }
  cTensor.get(iX,iY,iZ)[iA] /= (T)q;
}
```

# Algorithm

**One time step: $t\rightarrow t+\delta t$**

:::::::::::::: {.columns}
::: {.column width="28%"}
* CS $t\rightarrow t+\delta t$.
* 
* 
* 
* 
* 
* 
:::
::: {.column width="35%"}
* CS $t\rightarrow t+\frac{\delta t}{2}$.
* 
* 
* CS $t+\frac{\delta t}{2}\rightarrow t+\delta t$.
* 
* 
* 
:::
::: {.column width="33%"}
* Time interpolation.
* Space interpolation.
* Complete fine.
* Space interpolation.
* Complete fine.
* Filter.
* Complete coarse.
:::
::::::::::::::

# The algorithm in Palabos

## Recursive algorithm

\footnotesize

```C++
void collideAndStream(plint iL) {
  lattice[iL].collideAndStream();  // collision coarse
  if (iL < (plint)(gridLevels.size()-1)) {
    // coarse to fine coupling
    lattice[iL].decomposeAndRescale(); // t+1, resc f_i in fine NTensor
    lattice[iL].timeInterp(); // interp at time t + 1/2
    collideAndStream(iL+1);  // collision fine t->t+1/2
    lattice[iL].spatialInterp(); // at time t+1/2
    lattice[iL+1].recompose() // fine lattice recomposed at t+1/2
    lattice[iL+1].executeProcessors(); // BC, stats, ...
    collideAndStream(iL+1);  // collision fine t+1/2->t+1
    lattice[iL].spatialInterp(); // at time t+1
    lattice[iL+1].recompose() // fine is OK
    lattice[iL+1].executeProcessors(); // BC, stats, ...
    // fine to coarse coupling
    lattice[iL+1].decomposeAndRescale(); // t+1, resc f_i in coarse NTensor
    lattice[iL].filter();
    lattice[iL].recompose(); // coarse is OK
  }
}
```

# Questions on the algorithm

\Huge Questions?

# Dipole: [Link](videos/anim_dip.avi)

## Counter--rotating velocities confined in a 2D box[^4]

![](figs/dipoleNorm.pdf){#fig:ftoc width=80%}

## Enstrophy with respect to time

* Measure the mean enstrophy $\left\langle\Omega\right\rangle(t) = \frac{1}{2}\int\omega^2(\ux,t)\mathrm{d}\ux$.
  ![](figs/dip_ens.pdf){#fig:dip_ens width=35%}

[^4]: H. Clercx and C. Bruneau, The normal and oblique collision of a dipole with a no-slip boundary, Computers \& Fluids, 35 (2006), pp. 245--279.

# Dipole: Results

## Re=5000

* Spectral enstrophy $5536$. 
* Value with 6 refinement levels $5500$.
* **Importance of the filtering!**
  
  ![](figs/sixLevel.svg){#fig:ftoc width=100%}

  
  

# Data analysis in Palabos

## MultiLevelScalar/Tensor fields

* Must have the same grid structure than `MultiLevel3D`.
* The interface is similar to `MultiBlock3D`.

## Processing functionals

* `Integrate`/`Apply` must specify grid level.
* `Reductive` must specify grid level and provide "container" for the result:
  * More data is generated for fine level than coarse (twice more).

# Grid generation (1/2)

## Spoiler

* Grid generation is a very complex topic.

## Grid density

* Simplification: offload to external tool.
* Grid density: Scalar field $\in [0,1]$.
* Generated manually or by analyzing "coarse" simulation:
  * Typically more points where the are "large" gradients.

# Grid generation (2/2)

## Octree grid

* Start with cuboid.
* If grid density > threshold divide.
* Continue as long as the max number levels has not been reached.
* Balance the load on multiple processors.
* There are strong constraints:
  * Only factor of two at each interface.
  * Overlap blocks must be on the same processor as finer blocks.
  * Remove useless blocks (inside geometry).

# The end (tentative)

\Huge Questions?

# Exercises

* Three different exercises
  1. Familiarization with grid density.
  2. Generate alternative grid densities.
  3. Add functionalities for external flow simulations.
* Inspire yourself from existing code to add novel functionalities:
  * Read the code and understand it.
  * Develop news functionalities.
* Look for `Exercise` comments to find where to add functionalities.

# Exercises: Grid density (1/2)

## Boxes

* Add/remove boxes to see how grid densities are built.
* Visualize grid density fields.
  ![](figs/boxes.png){#fig:boxes width=60%}
* Modify the `simpleSphere_exercise.xml` to add boxes.

# Exercises: Grid density (2/2)

## Spheres

* Inspired by the Boxes code create spherical grid densities.
* Look for `Exercise` comments to find the relevant places to add code.
  ![](figs/sphere.png){#fig:spheres width=60%}
* Modify the `generateGridDensityFromSpheres_exercise.cpp` to add spherical shapes grid density.


# Exercises: flow past a sphere (1/2)

## Reynolds stress definition

* Reynolds decomposition
  $$
  \uu=\bar {\uu}+\uu',
  $$
  $\bar\uu$ and $\uu'$ mean and fluctuating part.
* Reynolds stress tensor
  $$
  \uT=\overline{\uu'\uu'}.
  $$

## Add Reynolds stress

* Compute $\bar{\uu}$ by averaging $\uu$ over time.
* Once $\bar{\uu}$ has converged:
  * Compute $\uu'=\uu-\bar{\uu}$.
  * Compute $\uu'\uu'$.
  * Average $\uu'\uu'$ over time to get $\uT$.
* Need `integrateProcessingFunctional`.

# Exercises: flow past a sphere (2/2)

## Add probes

* Want to measure values on special places.
* Use existing probes of Palabos: reductions.
* Input: position. Output: velocity, pressure, vorticity, ...
* Difficulty, one must decide on which level to apply the probes and store them.

# The real end

\Huge Questions?

<!-- # Grid-refinement in the lattice Boltzman method (2/N)

\begin{frame}{Dipole\href{videos/anim_dip.avi}{\beamergotobutton{Link}}}
Counter--rotating velocities confined in a 2D box\footnote{{\sc H.~Clercx and C.~Bruneau}, {\em The normal and oblique collision of a
  dipole with a no-slip boundary}, Computers \& Fluids, 35 (2006),
  pp.~245--279.}
\begin{figure}[!ht]
    \centering
    \includegraphics[scale=.23]{figs/dipoleNorm.pdf}
  \end{figure}
  \begin{columns}
  \begin{column}{0.5\linewidth}
  \begin{itemize}

\end{itemize}
  \end{column} 
  
  \begin{column}{0.5\linewidth}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=\linewidth]{figs/dip_ens.pdf}
  \end{figure}
  \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Dipole: Results}
$\Re=625$
\begin{itemize}
\item Three levels of refinement 
\begin{figure}[!ht]
\centering
\includegraphics[width=0.35\linewidth]{figs/dipole.pdf}\includegraphics[width=0.5\linewidth]{figs/errorFilteredUnfiltered.pdf}
\end{figure}
\item Large velocity gradients cross the refinement.
\item Time less points than uniform grid.
\item Three times faster.
\item Order two convergence is maintained.
\end{itemize}

\end{frame}



 -->
