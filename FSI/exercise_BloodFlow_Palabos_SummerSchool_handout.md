# Exercise on Blood Flow with Deformable Bodies

Goal of this exercise is to familiarize yourself with the bloodFlowDefoBodies Palabos application (found in ```palabos_root_dir/examples/showCases```). This application combines Palabos with npFEM library (found in ```palabos_root_dir/coupledSimulators folder```).

npFEM is a FEM (Finite Elements Method) solver for resolving the trajectories and deformations of bodies. Its main focus is blood cells, but it could readily be adapted to other case studies.

The bloodFlowDefoBodies application simulates cellular blood flow with deformable blood cells (Red Blood Cells/RBCs and Platelets/PLTs). Even if the focus of the application is quite specialized, we will see how to implement fluid-solid/structure interaction in Palabos, and these principles apply to any other applications with minor modifications.

The exercise can be done in both Linux and Windows. If you have a CUDA-capable workstation, you can enable the CUDA-backend of npFEM. To execute the application with MPI, use ```mpirun``` in Linux and ```mpiexec``` in Windows.

## Tasks

1. Go to ```palabos_root_dir/examples/showCases/bloodFlowDefoBodies```, read the instructions in the README file and compile the application. If you have a CUDA capable workstation (compute capability >6.0, check it using the deviceQuery CUDA example), you can manually enable CUDA support through CMake (npFEM solver can solve bodies in NVIDIA GPUs).

2. Perform Cell Packing (see README & cellPacking_params.xml), i.e., randomly position RBCs/PLTs and let npFEM solver to resolve collisions/penetrations. The Cell Packing will give an initial state for performing simulations under various flow conditions (e.g., shear or Poiseuille), at a later stage. By enabling Cell Packing in the application, there are parts of the code that are de-activated. Locate these parts in the code and discuss/understand them. Change the cellPacking_params.xml by increasing the hematocrit (RBC volume fraction) to 35% and run the application for 500 time steps (not enough to resolve penetrations but visualize the vtk files). Given the high computational cost (considering that you run it in a laptop), change the computational domain to a smaller one (box20x20x20 um^3). If the collisions are not resolved accurately which variable would you tune first? Do the appropriate changes in the cellPacking_params.xml and perform a cell packing in a tube (change the Simulation_Type to Poiseuille_Flow, etc.). For hints, check the poiseuille_params.xml file. ***Normally, to resolve all collisions/penetrations you need to run the application for >~5k time-steps (for high ht).***

3. Use the obstacle_params.xml, run the application (until the end, default parameters) and visualize the results by opening the produced vtk files in the tmp folder. You can modify the RBC initial position through the initialPlacing.pos file (try it by changing the RBC orientation). Furthermore, experiment with the Calpha RBC parameter (from 0.9 to 0.4, viscoelasticity) and observe the new outcome.

4. Extract the CPs_shear.tar.gz, which provides an initial field with RBCs & PLTs, and perform a simulation using the shear_params.xml (just few hundrend time-steps). Modify the output_timestep, to see the output more often. What does it change from the Cell Packing experiment, i.e., which parts of the code are active that are not in Cell Packing? Do the same for CPs_poiseuille.tar.gz using poiseuille_params.xml. Go through the code and locate the differences on Palabos side for generating Shear and Poiseuille Flow.
